import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { createStore} from 'redux'
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage';
import rootReducer from './reducers';
import TodoList from './components/TodoList';


const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['todoList'] // which reducer want to store
  //blacklist: ['todoList']
};

const pReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(
  pReducer
)
const persistor = persistStore(store);

const  App = () => {
  
  const clearPersistStorage = () => {
    persistor.purge().then(() => {
      console.log('----cleaned------')
    })
  }

  return (
    <Provider store={store}>
       <PersistGate persistor={persistor}>
            <TodoList />
             <hr />
            <button onClick={() => clearPersistStorage()}>Clear TodoList</button>
        </PersistGate>
    </Provider>
  );
}

export default App;
