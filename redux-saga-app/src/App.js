import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas';
import rootReducer from './reducers';
import PostsList from './components/PostsList';

const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))

sagaMiddleware.run(rootSaga)

const  App = () => {
  return (
    <>
      <Provider store={store}>
          <PostsList />
      </Provider>
    </>
  );
}

export default App;
