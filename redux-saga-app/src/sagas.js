import { put, takeEvery, all, call } from 'redux-saga/effects'
import {GET_POSTS,GET_POSTS_SUCCESS} from './types'
import axios from "axios";

function* getPostsSaga() {
  const response = yield call(
    axios.get,
    "https://jsonplaceholder.typicode.com/posts"
  );
  yield put({ type: GET_POSTS_SUCCESS, payload: response.data })
}

function* watchPosts() {
  console.log('----saga watcher---')
  yield takeEvery(GET_POSTS, getPostsSaga)
}

export default function* rootSaga() {
  yield all([
    watchPosts()
  ])
}