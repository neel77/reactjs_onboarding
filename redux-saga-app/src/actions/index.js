import {ADD_POST, GET_POSTS, DELETE_POST} from '../types';

export const getPosts = () => ({
    type: GET_POSTS
})

export const addPost = (post) => ({
    type: ADD_POST,
    payload: post
})

export const deletePost = (postId) => ({
    type: DELETE_POST,
    payload: postId
})
