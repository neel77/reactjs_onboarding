import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import { useState } from 'react';
import { addPost, deletePost, getPosts } from '../actions';

const PostsList = ({posts, addPost, deletePost, getPosts}) => {
    const [postTitle, setPostTitle] = useState('');
     
    useEffect(() => {
       getPosts();
    }, []);

    return (
      <>
       {posts.length > 0 ?
         <div>
             <ul>
                 {posts.map(post => (
                    <li key={post.id}>{post.title}&nbsp; <button onClick={() => deletePost(post.id)}>Delete Post</button> </li> 
                 ))}         
             </ul>
         </div> :
         <div>
             <h4>No Posts</h4>
         </div>
       }
       <input type="text" onChange={e => setPostTitle(e.target.value)} value={postTitle}></input>{'  '}
       <button onClick={() => addPost(postTitle)}>Add Post</button>
      </>
    );
}

const mapStateToProps = state => {
    return {
        posts: state.postsList.posts
    };
};

export default connect(mapStateToProps, {getPosts, addPost, deletePost})(PostsList);