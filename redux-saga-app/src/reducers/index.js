import {ADD_POST, GET_POSTS_SUCCESS, DELETE_POST} from '../types';
import {combineReducers} from 'redux';

const postsReducer = (state = { posts: [] } , action) => {
    switch(action.type){ 
        case GET_POSTS_SUCCESS:
            return {...state, posts: action.payload}
        case ADD_POST:
            return {...state, posts: [...state.posts, {id: state.posts.length + 100 , title: action.payload}]}
        case DELETE_POST:
            return {...state, posts: state.posts.filter(post => post.id !== action.payload)}
        default:
            return state;
    }
};

// Root Reducers
const rootReducer = combineReducers({
   postsList: postsReducer
});

export default rootReducer;