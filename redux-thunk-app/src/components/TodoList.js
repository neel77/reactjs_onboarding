import React from 'react';
import {connect} from 'react-redux';
import { useState } from 'react';
import { addTodo, deleteTodo } from '../actions';

const TodoList = ({todos,addTodo, deleteTodo}) => {
    const [todoText, setTodoText] = useState('');
     
    return (
      <>
       {todos.length > 0 ?
         <div>
             <ul>
                 {todos.map(todo => (
                    <li key={todo.todoId}>{todo.todoItem}&nbsp; <button onClick={() => deleteTodo(todo.todoId)}>Delete Todo</button> </li> 
                 ))}         
             </ul>
         </div> :
         <div>
             <h4>No Todos Added</h4>
         </div>
       }
       <input type="text" onChange={e => setTodoText(e.target.value)} value={todoText}></input>{'  '}
       <button onClick={() => addTodo(todoText)}>Add Todo</button>
      </>
    );
}

const mapStateToProps = state => {
    return {
        todos: state.todoList.todos
    };
};

export default connect(mapStateToProps, {addTodo, deleteTodo})(TodoList);