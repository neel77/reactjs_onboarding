import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import TodoList from './components/TodoList';

const store = createStore(
  rootReducer,
  applyMiddleware(thunk)
)

const  App = () => {
  return (
    <Provider store={store}>
      <TodoList />
    </Provider>
  );
}

export default App;
