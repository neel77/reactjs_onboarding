import {ADD_TODO, DELETE_TODO} from '../types';
import {combineReducers} from 'redux';

const todoReducer = (state = { todos: [] } , action) => {
    switch(action.type){ 
        case ADD_TODO:
            return {...state, todos: [...state.todos, {todoId: state.todos.length + 1 ,todoItem: action.payload}]}
        case DELETE_TODO:
            return {...state, todos: state.todos.filter(todo => todo.todoId !== action.payload)}
        default:
            return state;
    }
};

// Root Reducers
const rootReducer = combineReducers({
   todoList: todoReducer
});

export default rootReducer;