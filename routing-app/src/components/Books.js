import React from 'react';
import ReusableListView from './ReusableListView';

const BookList = [
     {value: 'book1', text: 'Book1'},
     {value: 'book2', text: 'Book2'},
     {value: 'book3', text: 'Book3'}
];

const BookList1 = [
    {value: 'book1_v1', text: 'Book1_V1'},
    {value: 'book2_v2', text: 'Book2_V2'},
    {value: 'book3_v3', text: 'Book3_V3'}
];

const Books = props => {
    return (
      <>
       <ReusableListView 
          list={BookList} 
          renderList={selectedValue => {
              if(selectedValue) {
                return <div>Selected Book: {selectedValue}</div>;
              }
          }}
        />
      </>
    );
}

export default Books;