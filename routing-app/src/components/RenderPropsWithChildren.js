import React from 'react';

const RenderPropsWithChildren = (props) => {
    return (
        props.children()
    );
}

export default RenderPropsWithChildren;