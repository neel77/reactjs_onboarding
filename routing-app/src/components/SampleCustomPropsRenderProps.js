import React from 'react';
import RenderPropsCustomProps from './RenderPropsCustomProps';

const SampleCustomPropsRenderProps = () => {
    return (
        <RenderPropsCustomProps renderList={() => {
            return (
             <h3>From Render Props Custom Props</h3>
            );
        }}
        />
    );
}

export default SampleCustomPropsRenderProps;