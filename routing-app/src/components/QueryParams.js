import React from "react";
import {
  BrowserRouter as Router,
  Link,
  useLocation
} from "react-router-dom";

export default () => {
  return (
    <Router>
      <QueryParamsDemo />
    </Router>
  );
}

const useQuery = () => {
  return new URLSearchParams(useLocation().search);
}

const QueryParamsDemo = () => {
  let query = useQuery();

  return (
    <div>
      <div>
        <h2>Query Params</h2>
        <ul>
          <li>
            <Link to="/query?name=query1">Query 1</Link>
          </li>
          <li>
            <Link to="/query?name=query21&name1=query22">Query 2</Link>
          </li>
        </ul>

        <Query name={query.get("name")} name1={query.get("name1")} />
      </div>
    </div>
  );
}

const Query = ({ name, name1 }) =>  {
  return (
    <div>
      {name ? (
        <h4>
          The name in the query string is &quot;{name}
      &quot;&nbsp;  { name1 ? '"' + name1 + '"' : null}
        </h4>
      ) : (
        <h4>There is no name in the query string</h4>
      )}
    </div>
  );
}
