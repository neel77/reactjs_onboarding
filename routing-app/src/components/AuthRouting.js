import React, { useState } from "react";
import {BrowserRouter as Router, Switch, Route, Link, useHistory, useLocation } from "react-router-dom";
import PrivateRoute from './PrivateRoute';

const AuthRouting = () => {
  const [auth, setAuth] = useState(localStorage.getItem('auth') !== null);

  const signout = () => {
    localStorage.removeItem('auth')
    setAuth(false)
  }

  return (
    <Router>
      <div>
       <h1>Authenticated Routing</h1>
       {auth ? 
              <>
              <h4>You are logged in </h4> 
              &nbsp;
              <button onClick={signout}>Sign Out</button>
              </>  :
              <h4>You are not logged in</h4> 
        }
        <ul>
          <li>
            <Link to="/public">Public Page</Link>
          </li>
          <li>
            <Link to="/protected">Protected Page</Link>
          </li>
        </ul>
        <Switch>
          <Route path="/public">
            <PublicPage />
          </Route>
          <Route path="/login">
            <LoginPage render={(auth) => {
                if(auth) {
                    setAuth(true)
                }
            }} />
          </Route>
          <PrivateRoute path="/protected">
            <ProtectedPage />
          </PrivateRoute>
        </Switch>
      </div>
    </Router>
  );
}

export default AuthRouting;

const PublicPage = () => {
    return (
       <h3>Public</h3>
    );
}

const ProtectedPage = () => {
    return (
       <h3>Protected</h3>
    );
}

const LoginPage = (props) => {
    let auth = false;
    let history = useHistory();
    let location = useLocation();

    let { from } = location.state || { from: { pathname: "/" } };

    const loginClick = () => {
        localStorage.setItem('auth', 'success');
        auth = true;
        props.render(auth);
        history.replace(from);
    }

    return (
        <div>
          <p>You must log in to view the page</p>
          <button onClick={loginClick}>Log in</button>
        </div>
    );
}