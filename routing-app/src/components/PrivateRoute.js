import React from 'react';
import { Route, Redirect} from 'react-router-dom';

const PrivateRoute = ({children, path}) => {

    return (
        <Route
            path={path}
            component={({location}) =>
               localStorage.getItem('auth') ? children : <Redirect to={{ 
                   pathname: "/login",
                   state: { from: location }
                }} />
            }
        />

    );
} 

export default PrivateRoute;