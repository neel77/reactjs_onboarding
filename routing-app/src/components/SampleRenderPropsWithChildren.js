import React from 'react';
import RenderPropsWithChildren from './RenderPropsWithChildren';

const SampleRenderPropsWithChildren = () => {
    return (
        <RenderPropsWithChildren>
           {() => {
            return (
             <h3>From Render Props With Children</h3>
            );
        }}
        </RenderPropsWithChildren>
    );
}

export default SampleRenderPropsWithChildren;