import React from "react";
import {BrowserRouter as Router, Switch, Route, Link, useParams,useRouteMatch, useLocation } from "react-router-dom";

const  BasicRouting = () => {
  return (
    <Router>
      <div>
       <h1>Basic Routing</h1>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link to="/comp1/2/3">Component1</Link>
          </li>
          <li>
            <Link to="/topics">Topics(Nested Routes)</Link>
          </li>
          <li>
            <Link to="/no-match">NoMatch</Link>
          </li>
        </ul>

        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/topics">
            <Topics />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route 
            path="/comp1/:id1/:id2"
            component={(props) => <Component1 {...props} />} //Route component defines its own props.These props are called history, match and location.       
          />
          <Route  path="*">
             <NoMatch />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default BasicRouting;

const  Home = () => {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

const  About = () => {
  return (
    <div>
      <h2>About</h2>
    </div>
  );
}

const Dashboard = () => {
  return (
    <div>
      <h2>Dashboard</h2>
    </div>
  );
}

const Component1 = ({match}) => {
    //let {id1, id2} = useParams();
     let {id1, id2} = match.params;
    return (
        <div>
            <h3>Component1 Id: {id1}&nbsp;{id2}</h3>
        </div>
    )
}

const  Topics = () => {
  let { path, url } = useRouteMatch();
  console.log(path, url)
  console.log(useRouteMatch())

  return (
    <div>
      <h1>Nested Routes</h1>
      <h2>Topics</h2>
      <ul>
        <li>
          <Link to={`${url}/topic1`}>Topic1</Link>
        </li>
        <li>
          <Link to={`${url}/topic2`}>Topic2</Link>
        </li>
        <li>
          <Link to={`${url}/topic3`}>Topic3</Link>
        </li>
      </ul>

      <Switch>
        <Route exact path={path}>
          <h3>Please select a topic.</h3>
        </Route>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

const Topic = () => {
  let { topicId } = useParams();
  console.log('Topic',useRouteMatch())
  /* Output if topic1 is selected
        isExact: true 
        params: {topicId: "topic1"} 
        path: "/topics/:topicId" 
        url: "/topics/topic1" */
  return (
    <div>
      <h3>{topicId}</h3>
    </div>
  );
}

const NoMatch = () => {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}