import React from 'react';
import RenderPropsElement from './RenderPropsElement';

const SampleRenderProps = () => {
    return (
        <RenderPropsElement render={() => {
            return (
             <h3>From Render Props Element</h3>
            );
        }}
        />
    );
}

export default SampleRenderProps;