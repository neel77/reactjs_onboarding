import React, { useState } from 'react';

const ReusableListView = ({list}) => {
    const [selectedValue, setSelectedValue] = useState('');

    return (
      <>
       <ul>
            {list && list.map(item => {
                return( 
                 <li style={{cursor: 'pointer'}} onClick={() => setSelectedValue(item.value)}>
                       {item.text}
                 </li>
                );
            })}
       </ul>
       Selected Value: {selectedValue}
      </>
    );
}

export default ReusableListView;