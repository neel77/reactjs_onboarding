import React from 'react';

const RenderPropsElement = (props) => {
    return (
        props.render()
    );
}

export default RenderPropsElement;