import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

export default () => {
    const routes = [
        {
          path: "/route1",
          component: Route1
        },
        {
          path: "/nested-route",
          component: NestedRoute,
          routes: [
            {
              path: "/nested-route/route1",
              component: SubRoute1
            },
            {
              path: "/nested-route/route2",
              component: SubRoute2
            }
          ]
        }
    ];

   return (
    <Router>
      <div>
        <h2>Routing Config</h2>  
        <ul>
          <li>
            <Link to="/nested-route">Nested Route</Link>
          </li>
          <li>
            <Link to="/route1">Route1</Link>
          </li>
        </ul>

        <Switch>
          {routes.map((route, i) => (
            <RouteWithSubRoutes key={i} {...route} />
          ))}
        </Switch>
      </div>
    </Router>
  );
}

// A special wrapper for <Route> that knows how to
// handle "sub"-routes by passing them in a `routes`
// prop to the component it renders.
const  RouteWithSubRoutes = (route) => {
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

const Route1 = () => {
  return <h2>Route1</h2>;
}

const  NestedRoute = ({ routes }) => {
  return (
    <div>
      <h2>Nested Route</h2>
      <ul>
        <li>
          <Link to="/nested-route/route1">Nested Route1</Link>
        </li>
        <li>
          <Link to="/nested-route/route2">Nested Route2</Link>
        </li>
      </ul>

      <Switch>
        {routes.map((route, i) => (
          <RouteWithSubRoutes key={i} {...route} />
        ))}
      </Switch>
    </div>
  );
}

const SubRoute1 = () => {
  return <h3>Nested Route1</h3>;
}

const SubRoute2 = () => {
  return <h3>Nested Route2</h3>;
}