import React from 'react';
import './App.css';
import BasicRouting from './components/BasicRouting';
import SampleRenderProps from './components/SampleRenderProps';
import SampleCustomPropsRenderProps from './components/SampleCustomPropsRenderProps';
import SampleRenderPropsWithChildren from './components/SampleRenderPropsWithChildren';
import Books from './components/Books';
import AuthRouting from './components/AuthRouting';
import QueryParams from './components/QueryParams';
import FinalRouting from './components/FinalRouting';

const App = () => {
  return (
    <>
     <BasicRouting />
     <hr />
     <AuthRouting />
     <hr />
     <QueryParams />
     <hr />
     <FinalRouting />
     <hr />
     <SampleRenderProps />
      <hr />
      <SampleCustomPropsRenderProps />
      <hr />
      <SampleRenderPropsWithChildren />
      <hr />
      <Books />
    </>
  );
}

export default App;
