import {ADD_NUMBER, SUBTRACT_NUMBER} from '../types';

export const addAction = () => ({
    type: ADD_NUMBER,
    payload: 1,
});

export const subtractAction = () => ({
    type: SUBTRACT_NUMBER,
    payload: 1,
});