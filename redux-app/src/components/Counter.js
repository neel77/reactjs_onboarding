import React from 'react';
import {connect} from 'react-redux';
import {addAction, subtractAction} from '../actions';

const Counter = ({count, addAction, subtractAction}) => (
    //OR onClick={addAction}
    <div>
        <h2>Counter: {count} </h2>
        <input type='button' value='add' onClick={() => addAction()}/>&nbsp;
        <input type='button' value='subtract' onClick={() => subtractAction()}/>
    </div>
);

const mapStateToProps = state => {
   return {
       count : state.math.number
   }
}

// // mapDispatchToProps
// const mapDispatchToProps = dispatch => ({
//     add: () => dispatch(addAction()),
//     subtract: () => dispatch(subtractAction()),
// });

export default connect(mapStateToProps, {addAction, subtractAction})(Counter);