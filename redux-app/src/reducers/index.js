import {ADD_NUMBER, SUBTRACT_NUMBER} from '../types';
import {combineReducers} from 'redux';

const mathReducer = (state = {number: 0}, action) => {
    if (action.type === ADD_NUMBER) {
        console.log('-----add Reducer',action)
        return {...state, number: state.number + action.payload};
    }
    else if (action.type === SUBTRACT_NUMBER) {
        return {...state, number: state.number - action.payload};
    }
    return state;
};

// Root Reducers
const rootReducer = combineReducers({
   math: mathReducer,
});

export default rootReducer;