import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import './App.css';
import SignupFormWithFormik from './components/SignUpFormWithFormik';
import SignUpFormWithFormik2 from './components/SignUpFormWithFormik2';
import RegisterForm from './components/RegisterForm';

function App() {
  return (
    <>
       <Router>
        <div>
            <ul>
              <li>
                <Link to="/">Register with Formik</Link>
              </li>
              <li>
                <Link to="/signup-with-formik">Sign Up using useFormik hooks in Formik</Link>
              </li>
              <li>
                <Link to="/sign-up-with-formik2">Sign Up using Formik</Link>
              </li>
            </ul>

            <Switch>
              <Route exact path="/">
                <RegisterForm />
              </Route>
              <Route path="/signup-with-formik">
                <SignupFormWithFormik />
              </Route>
              <Route path="/sign-up-with-formik2">
                <SignUpFormWithFormik2 />
              </Route>
            </Switch>
          </div>
       </Router>
    </>
  );
}

export default App;
