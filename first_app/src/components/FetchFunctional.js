import React, {useState, useEffect} from 'react';

const  FetchFunctional = () => {
    const [error, setError] = useState(null);
    const [items, setItems] = useState([]);
  
    useEffect(() => {
      fetch("https://jsonplaceholder.typicode.com/posts")
        .then(res => res.json())
        .then(
          (result) => {
            setItems(result);
          },
          (error) => {
            setError(error);
          }
        )
    }, [])
  
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <ul>
          {items.map(item => (
            <li key={item.id}>
              {item.id}-&nbsp;{item.title} 
            </li>
          ))}
        </ul>
      );
    }
  }

export default FetchFunctional;