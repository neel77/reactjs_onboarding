import React from 'react'

class FetchClass extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }
  
    componentDidMount() {
      fetch("https://jsonplaceholder.typicode.com/posts")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              items: result
            });
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              error
            });
          }
        )
    }
  
    render() {
      const { error, items } = this.state;
      if (error) {
        return <div>Error: {error.message}</div>;
      } else {
        return (
          <ul>
            {items.map(item => (
              <li key={item.id}>
                {item.id}-&nbsp; {item.title}
              </li>
            ))}
          </ul>
        );
      }
    }
  }

  export default FetchClass;