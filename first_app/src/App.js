import React from 'react';
import logo from './logo.svg';
import './App.css';
import FetchFunctional from './components/FetchFunctional';
import FetchClass from './components/FetchClass';

function App() {
  return (
   <>
    <div>
        Using Functional Component
         <br />
        <FetchFunctional />
    </div>
    <div>
      Using Class Component
      <br />
      <FetchClass />
   </div>
   </>
  );
}

export default App;
