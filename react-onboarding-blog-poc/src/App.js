import React from 'react';
import './App.css';
import Routes from '../src/routes';
import {BrowserRouter as Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../src/reducers';
import thunk from 'redux-thunk';

function App() {
  return (
    <>
      <Provider store={createStore(rootReducer, applyMiddleware(thunk))}>
        <Router>
            <Routes />
        </Router>
       </Provider>
    </>
  );
}

export default App;
