export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const GET_POSTS = 'GET_POSTS';
export const ADD_POST = 'ADD_POST';
export const GET_POST_DETAILS = 'GET_POST_DETAILS';
export const GET_COMMENTS = 'GET_COMMENTS';
export const ADD_COMMENT = 'ADD_COMMENT';