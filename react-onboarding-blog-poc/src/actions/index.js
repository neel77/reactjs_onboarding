import {LOGIN_SUCCESS, LOGOUT_SUCCESS, GET_POSTS, GET_POST_DETAILS, GET_COMMENTS, ADD_COMMENT, ADD_POST} from '../types';
import axios from 'axios';

export const loginUser = (username,password) => {
    if(username === 'user1' &&  password === 'user123') {
        return {
            type: LOGIN_SUCCESS,
            payload: {user : 'user1'}
        }
    }
}

export const logoutUser = () => {
    localStorage.removeItem('user');
    return {
        type: LOGOUT_SUCCESS
    }
};

export const getPosts = () => {
     return (dispatch) => {
        axios.get('https://jsonplaceholder.typicode.com/posts').then( res => {
            dispatch({
                type: GET_POSTS,
                payload: res.data
            })
        })
    }
}

export const addPost = (title, desc) => {
    return {
        type: ADD_POST,
        payload: {title, desc}
    }
}

export const getPostDetails = (id) => {
     return (dispatch) => {
        axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`).then( res => {
            dispatch({
                type: GET_POST_DETAILS,
                payload: res.data
            })
        })
    }
}

export const getComments = (id) => {
    return (dispatch) => {
       axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${id}`).then( res => {
           dispatch({
               type: GET_COMMENTS,
               payload: res.data
           })
       })
   }
}

export const addComment = (user, comment) => {
    return {
        type: ADD_COMMENT,
        payload: { user, comment}
    }
}