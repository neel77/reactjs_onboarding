import React, { useEffect } from 'react';
import {connect} from 'react-redux';
import {useParams} from 'react-router-dom';
import {getPostDetails} from '../actions';
import '../styles/PostDetails.css';
import Comments from './Comments';

const PostDetails = ({getPostDetails, postDetails}) => {
  const {id} = useParams();
  
  useEffect(() => {
      getPostDetails(id); 
  }, []);
    
  return (
    <>
       <h3 className="post-title">{postDetails.title}</h3>
       <div className="post-body">{postDetails.body}</div>
       <Comments id={id} />
    </>
  );
}

const mapStateToProps = state => {
  return {
    postDetails: state.posts.postDetails
  }
}

export default connect(mapStateToProps, {getPostDetails})(PostDetails);