import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useHistory } from 'react-router-dom';
import {connect} from 'react-redux';
import {logoutUser} from '../actions';
import AddPost from './AddPost';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      cursor: 'pointer'
    },
}));

const Header = ({auth, user, logoutUser}) =>  {
  let history = useHistory();
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const logout = () => {
    logoutUser();
    history.push('/posts-list');
 }

 const dashboard = () => {
    history.push('/posts-list')
 }

  return (
    <>
      <AppBar position="static">
          <Toolbar>
              <Typography variant="h6" className={classes.title} onClick={dashboard}>
                  BLOGS
              </Typography>
              { 
                auth ? 
                <>
                  <Button color="inherit" onClick={handleClickOpen}>Add Post</Button>
                  <Button color="inherit" onClick={logout}>Logout<span>({user})</span></Button>
                </>
                : null
              }
          </Toolbar>
      </AppBar>
      <AddPost open={open} handleClose={handleClose} />
    </>
  );
}

const mapStateToProps = state => {
   return {
     auth : state.auth.auth,
     user : state.auth.user
   }
}

export default connect(mapStateToProps, {logoutUser})(Header);
