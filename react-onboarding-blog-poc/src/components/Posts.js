import React, { useEffect, useState } from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import { getPosts } from '../actions';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader'
import IconButton from '@material-ui/core/IconButton';
import ArrowIcon from '@material-ui/icons/ArrowForward';
import Pagination from './Pagination';

const useStyles = makeStyles({
  root: {
    width: 1200,
    marginTop: 20,
    marginLeft: 20
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  arrowStyle: {
    marginLeft: 'auto'
  }
});

const Posts = ({getPosts, posts}) => {
   const classes = useStyles();   
   const [currentPage,setCurrentPage] = useState(1);
   const postsPerPage = 10;
   const history = useHistory();

    useEffect(() => {
       getPosts();
    }, []);

    const indexOfLastPost = currentPage * postsPerPage;
    const indexOfFirstPost = indexOfLastPost - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);
    
    const paginate = pageNum => setCurrentPage(pageNum);
    const nextPage = () => setCurrentPage(currentPage + 1);
    const prevPage = () => setCurrentPage(currentPage - 1);

    return (
      <>
       {posts.length > 0 ?
         <div>
              {currentPosts.map(post => (
                  <Card className={classes.root} key={post.id}>
                  <CardHeader title={post.title}/>
                  <CardContent>
                     {post.body}
                  </CardContent>
                  <CardActions>
                      <IconButton className={classes.arrowStyle} onClick={() => history.push(`/posts-list/${post.id}`)}>
                         <ArrowIcon />
                      </IconButton>
                  </CardActions>
                </Card>
              ))}  
              <Pagination postsPerPage={postsPerPage} totalPosts={posts.length} paginate={paginate} nextPage={nextPage} prevPage={prevPage} />       
         </div> :
         <div>
             <h4>Loading...</h4>
         </div>
       }
      </>
    );
}

const mapStateToProps = state => {
    return {
        posts: state.posts.posts
    };
};

export default connect(mapStateToProps, {getPosts})(Posts);