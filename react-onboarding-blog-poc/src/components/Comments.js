import React, { useEffect, useState } from 'react';
import {connect} from 'react-redux';
import {getComments, addComment} from '../actions';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import '../styles/Comments.css';

const useStyles = makeStyles({
    root: {
      width: 800,
      marginTop: 20,
      marginLeft: 20,
      marginBottom: 20
    },
    commentInput: {
       margin: 20,
       '& .MuiTextField-root': {
        width: 500,
      },
    },
    commentButton: {
        marginTop: 20
    }
});

const Comments = ({id, auth, user, comments, getComments, addComment}) => {
  const classes = useStyles();
  const [values, setValues] = useState({comment: ''});

  useEffect(() => {
      getComments(id);
  }, []);

  const handleChange = e => {
    setValues({ ...values, comment: e.target.value });
  };

  return (
    <>
        {  
          comments.length > 0  ?
           <>
               <h5 className="comments-header">Comments</h5> 
                {comments.map(comment => (
                    <Card className={classes.root} key={comment.id}>
                    <CardHeader title={comment.email}/>
                        <CardContent>
                            {comment.body}
                        </CardContent>
                    </Card>
                ))}
            </>
           :
            <div>
                <h4>Loading...</h4>
            </div> 
        }
        { auth ? 
            <div className={classes.commentInput}>
                <TextField
                    id="standard-textarea"
                    label="Comment"
                    placeholder="Add a Comment"
                    multiline
                    rowsMax={4}
                    value={values.comment}
                    onChange={e => handleChange(e)}
                />&nbsp;
                <Button 
                    color="primary" 
                    className={classes.commentButton} 
                    disabled={values.comment === ''}
                    onClick={() => {setValues({ ...values, comment: '' });addComment(user, values.comment)}}>
                    Comment
                </Button>
            </div> : null
        }  
    </>
  );
}

const mapStateToProps = state => {
    return {
        auth: state.auth.auth,
        user: state.auth.user,
        comments : state.posts.comments
    }
}

export default connect(mapStateToProps, {getComments, addComment})(Comments);
