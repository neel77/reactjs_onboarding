import React, {useState} from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import '../styles/Login.css';
import { useHistory } from 'react-router-dom';
import {connect} from 'react-redux';
import {loginUser} from '../actions';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const  Alert = (props)=> {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Login = ({loginUser, auth}) =>  {
    let history = useHistory();  
    const [snackState, setSnackState] = useState(false);

    const handleClose = () => {
        setSnackState(false);
    };

    return (
      <> 
         {
           snackState ? 
           <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'right' }} open={snackState} autoHideDuration={2000} onClose={handleClose}>
             <Alert onClose={handleClose} severity="success">
                 Login Successful
             </Alert>
          </Snackbar> : null
         }
        <div className="form">  
        <h3 className="loginLabel">Login</h3>
          <div className="row">
                  <Formik
                      initialValues={{
                          userName: '',
                          password: '',
                      }}
                      validationSchema={Yup.object({
                          userName: Yup.string()
                              .required('Username is required'),
                          password: Yup.string()
                              .min(6, 'Password must be at least 6 characters')
                              .required('Password is required'),
                      })}
                      onSubmit={fields => {
                          localStorage.setItem('user', fields.userName)
                          loginUser(fields.userName, fields.password);
                          setSnackState(true);
                          setTimeout(() => {
                            history.push('/posts-list');
                          }, 1000);
                      }}
                  >
                      <Form>
                          <div className="form-group">
                              <label htmlFor="userName">Username</label>
                              <Field name="userName" type="text" className='form-control' />
                              <ErrorMessage name="userName" component="div" className="error"/>
                          </div>
                          <div className="form-group">
                              <label htmlFor="password">Password</label>
                              <Field name="password" type="password" className='form-control' />
                              <ErrorMessage name="password" component="div" className="error" />
                          </div>
                          <div className="form-group">
                              <button type="submit" className="btn btn-primary mr-2 buttons">Login</button>&nbsp;
                              <button type="reset" className="btn btn-danger buttons">Clear</button>
                          </div>
                      </Form>
                  </Formik>
              </div>
          </div>
      </>
    );
};

const mapStateToProps = state => {
    return {
        auth: state.auth.auth
    }
}

export default connect(mapStateToProps,{loginUser})(Login);