import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import {connect} from 'react-redux';
import {addPost} from '../actions';

const AddPost = ({open, handleClose, addPost}) =>  {
 const [form, setForm] = useState({title:'',desc:''});

 const handleChange = name => e => {
    setForm({ ...form, [name]: e.target.value });
  };

  return (
    <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add Blog Post</DialogTitle>
        <DialogContent>
            <TextField
                autoFocus
                margin="dense"
                id="title"
                required
                label="Title"
                type="text"
                fullWidth
                value={form.title}
                onChange={handleChange("title")}
            />
            <TextField
                margin="dense"
                id="desc"
                label="Description"
                required
                multiline
                fullWidth
                value={form.desc}
                onChange={handleChange("desc")}
            />
        </DialogContent>
        <DialogActions>
            <Button onClick={handleClose} color="primary">
                Cancel
            </Button>
            <Button 
                    onClick={() =>  {
                        addPost(form.title, form.desc); 
                        setForm({title:'', desc: ''}); 
                        handleClose();
                    }} 
                    color="primary" 
                    disabled={form.title === '' || form.desc === ''}>
                Post
            </Button>
        </DialogActions>
  </Dialog>
  );
}

export default connect(null, {addPost})(AddPost);