import {LOGIN_SUCCESS, LOGOUT_SUCCESS} from '../types';

export const AuthReducer = (state = {auth: (localStorage.getItem('user') !== null) , user: (localStorage.getItem('user') !== null) ? localStorage.getItem('user') : ''}, action) => {
    switch(action.type) {
        case LOGIN_SUCCESS: 
           return {...state, auth: true, user: action.payload.user}
        case LOGOUT_SUCCESS: 
           return {...state, auth: false, user: ''}
        default: 
           return state;
    }
};
