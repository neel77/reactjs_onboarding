import {GET_POSTS, ADD_POST, GET_POST_DETAILS, GET_COMMENTS, ADD_COMMENT} from '../types';

export const PostsReducer = (state = {posts: [], postDetails: {},comments: []}, action) => {
    switch(action.type) {
        case GET_POSTS: 
           return {...state, posts: action.payload}
        case ADD_POST:
            return {...state, posts: [...state.posts, {id: state.posts.length + 100 , title: action.payload.title, body: action.payload.desc}]}
        case GET_POST_DETAILS:
            return {...state, postDetails: action.payload}
        case GET_COMMENTS: 
            return {...state, comments: action.payload}
        case ADD_COMMENT:
            return {...state, comments: [...state.comments, {id: state.comments.length + 100 , body: action.payload.comment, email: action.payload.user}]}
        default: 
           return state;
    }
};
