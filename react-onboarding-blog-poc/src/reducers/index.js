import {combineReducers} from 'redux';
import {AuthReducer} from './AuthReducer';
import {PostsReducer} from './PostsReducer';

// Root Reducers
const rootReducer = combineReducers({
   auth: AuthReducer,
   posts: PostsReducer
});

export default rootReducer;