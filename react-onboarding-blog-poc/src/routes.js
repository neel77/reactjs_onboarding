import React from 'react';
import { Route, Switch, Redirect} from 'react-router-dom';
import Header from './components/Header';
import Login from './components/Login';
import Posts from './components/Posts';
//import Home from './components/Home';
import PrivateRoute from './PrivateRoute';
import PostDetails from './components/PostDetails';

const Routes = () => {
  return (
    <>
      <Header/>
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route exact path="/">
            <Redirect to="/posts-list" />
        </Route>
        <PrivateRoute exact path="/posts-list">
           <Posts />
        </PrivateRoute>
        <PrivateRoute exact path="/posts-list/:id">
           <PostDetails />
        </PrivateRoute>
      </Switch>
    </>
  );
};

export default Routes;