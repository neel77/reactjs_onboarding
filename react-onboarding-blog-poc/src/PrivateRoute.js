import React from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

const PrivateRoute = ({children, auth, ...rest}) => {
  return (
    <>
       <Route
            {...rest} >
            { auth ? 
                 children 
               : <Redirect to="/login" />
            }
        </Route>
    </>
  );
}

const mapStateToProps = state => {
    return {
        auth: state.auth.auth
    }
}

export default connect(mapStateToProps)(PrivateRoute);